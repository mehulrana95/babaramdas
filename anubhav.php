<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>।। बाबा रामदास समाधी मंदिर ।।</title>
<!-- Core CSS-->
<link href="css/global.css" rel="stylesheet">
<link href="css/jquery.bxslider.css" rel="stylesheet">
<!--Core Javascript-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.bxslider.js"></script>
<script src="js/main.js"></script>

<script>
$(document).ready(function(){  
  $('.slider').bxSlider();
});
	
</script>
</head>

<body id="insidepage">
	<div class="wrapper">
	<div class="header">
    	<script src="js/header.js"></script>
        <script>$(function(){$('.aAnubhav').addClass('active')})</script>
    </div>
    
    <div class="container">
		<div class="pageHeading"><h1>Anubhav</h1></div>
    	<div class="comingSoon"><h1 >Coming Soon</h1><div class="clear"></div><div class="emailUs" style="text-align:center;display:block">write us your Anubhav at <a href="mailto:info@babaramdas.org" style="color:#000">info@babaramdas.org</a></div></div>        
    </div>
    </div>
    <div class="clear"></div>
    <div class="footer">
    	<script src="js/footer.js"></script>
    </div>
</body>
</html>
