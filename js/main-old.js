function menuToggle() {
    if ($(window).width() < 1023) {
       $('ul.menus').hide();
        var state = 0;
        $('.nav-collapse').click(function() {
            if (state == 0) {
                state = 1;
                //$('ul.menu').animate({marginLeft:40 + '%'} ,200);
				$('ul.menus').slideDown();
                return false;
            } else if (state == 1) {
                state = 0;
                //$('ul.menu').animate({marginLeft:100 + '%'} ,200);
				$('ul.menus').slideUp();
                return false;
            }
        });
    } else {
       $('ul.menus').show();
    }
}

function changeDiv(){
	
	$( ".content .col2" ).insertAfter( ".content .col1:first-child" );
	 if ($(window).width() < 768) {
		 $( ".content .col2" ).insertBefore( ".content .col1:first-child" );
		 
		 }
		
	}
	
function divHeight(){
	var height= $('.group p').outerHeight();
	var height= $('.col1 ul').outerHeight();
	$('.col1 ul').css('height',height - 20)
	$('.group p').css('height',height - 20).css('overflow','auto');
	
	//var videoHeight = $('.streaming').outerHeight();
	var videoHeight = $('.photogallery img').outerHeight();
	$('.video img').css('height',videoHeight - 5);
	//$('.photogallery').css('height',videoHeight);
	
	}
	

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;

    return true;
}
$(document).ready(function(){
						   
					   
	menuToggle();
	divHeight();
   changeDiv();
   
   
   
   $('.gallery li a').hover(function(){
	//var alternateText = $(this).find('img').attr('alt'); 
	$(this).append('<div class="divcaption"></div>')
	$(this).children('img').css('transform', 'scale(1.2)');
	//$(this).find('.divcaption').fadeIn();
	
},
  function() {
   $('.divcaption').hide();
   $(this).children('img').css('transform', 'scale(1)');
  }
)
   $(".galleryTabs ul li a").each(function(index) {
        var count = index;
        $(this).click(function(e) {
            $(".galleryTabs ul li a").removeClass("active");
            $(this).addClass("active");
            $(this).parent().parent().parent().next().children().children().slideUp('slow');
            $("#gallery" + count).slideDown('slow').next().show();
			
        });
    });
   
   $('#btn1').click(function(){
		$('.bgPopup').fadeIn();
		$('#popup1').fadeIn();
		$('.close').fadeIn();
	})
   $('#btn2').click(function(){
		$('.bgPopup').fadeIn();
		$('#popup2').fadeIn();
		$('.close').fadeIn();
	})
   $('#btn3').click(function(){
		$('.bgPopup').fadeIn();
		$('#popup3').fadeIn();
		$('.close').fadeIn();
	})
   $('#btn4').click(function(){
		$('.bgPopup').fadeIn();
		$('#popup4').fadeIn();
		$('.close').fadeIn();
	})
    $('.btn5').click(function(){
		$('.bgPopup').fadeIn();
		$('#popup5').fadeIn();
		$('.close').fadeIn();
	})
	$('.btn5').click(function(){
		$('.bgPopup').fadeIn();
		$('#popup5').fadeIn();
		$('.close').fadeIn();
	})
    $('.close').click(function(){
		$('.bgPopup').fadeOut();
		$('.popup').fadeOut();
		$('.close').fadeOut();
	})
   
	$('.video a, .group a').click(function(){
		$('.bgPopup').fadeIn();
		$('.VideoPopup').fadeIn();
	})
	 $('.closebtn').click(function(){
		$('.bgPopup').fadeOut();
		$('.VideoPopup').fadeOut();
	})


	
})

$(window).resize(function(){
	menuToggle();
	divHeight();
	
	/*if($(window).width() >= 768)
	{
		location.reload();
	}*/
})