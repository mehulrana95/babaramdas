<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>।। बाबा रामदास समाधी मंदिर ।। </title>
<!-- Core CSS-->
<link href="css/global.css" rel="stylesheet">
<link href="css/jquery.bxslider.css" rel="stylesheet">
<!--Core Javascript-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.bxslider.js"></script>
<script src="js/main.js"></script>

<script>
$(document).ready(function(){  
  $('.slider').bxSlider();
});
	
</script>
</head>

<body>
<div class="mainImage"><img src="images/BABA-RAMDAS.jpg"></div>
<div  id="Homepage">
<div class="wrapper">
        <div class="header">
            <script src="js/header.js"></script>
            <script>$(function(){$('.aHome').addClass('active')})</script>
        </div>
    
        <div class="content">
            <div class="col1" id="column1">
                <div class="program">
                <h1>Program of the year</h1>
                <ul>
                    <li>Ustav</li>                
                    <li><a href="pdf/Patrika-Guru-Pornima-2015.pdf" target="_blank">Guru Poornima</a></li>
                    <li>Dev Diwali </li>
                </ul>
                <a href="program.html" class="btn">View All +</a>
                </div>
                <div class="group">
                    <h1>Hari Hari Group</h1>
                    <p>All Devotoes and followers can add
    their names here to receive all updates 
    on SMS and Email</p>
                <a href="#" class="btn">CLICK TO ADD YOUR DETAILS</a>
                </div>
            </div>
            <div class="col2" id="column2">
                
                 <div class="banner">
                    <img src="images/banner.jpg">
                    <img src="images/bg/bannerShadow.png" id="shadow">
                 </div>
                 
                 <div class="contentDetails">
                 	<p>Param Pujya Baba Ramdas Samadhi Mandir and Param Pujya Gharpure Saheb Samadhi Mandir is located at Shree Kshetra Garudeshwar, Post Mundhegaon,  Tal. Igatpuri, Dist - Nashik, Maharashtra State, India.</p>
<p>Baba Ramdas Maharaj Samadhi and his immediate disciple Gharpure Saheb Samadhi are in the same Mandir.</p>
<p>The main Utsav Celebrated at the Samadhi Mandir are :</p>
<p>Akhand Hari Namm Jagar to mark the Punyathithi Utsav of Baba Ramdas with seven days FREE Medical Camp for the poor and needy people from Nearby Villages and Palkhi coming from Lahe to the Samadhi Mandir and Bhandara on Punya Tithi (last) day.</p>
<p>Guru Poornima Sohala and Dev Diwali Utsav are celebrated and attended by many devotees coming from various parts of Mumbai & Maharashtra at the Samadhi Mandir every year.</p>
                    
                 </div>
                </div>
            <div class="col1" id="column3">
                <div class="streaming">
                    <h1><img src="images/icon/live-streaming.png"><span>Live Streaming</span></h1>
                    <div class="video"><a href="javascript:void()"><img src="images/video.jpg"></a></div>
                </div>
                <div class="photogallery">
                     <h1>Photo Gallery</h1>
                    <ul class="slider">
                        <li><img src="images/gallery/gallery1.jpg"></li>
                        <li><img src="images/gallery/gallery.jpg"></li>
                        <li><img src="images/gallery/gallery2.jpg"></li>
                        <li><img src="images/gallery/gallery3.jpg"></li>
                        <li><img src="images/gallery/gallery4.jpg"></li>
                        <li><img src="images/gallery/gallery5.jpg"></li>
                    </ul>
                    <div style="margin-top:-15px"><a href="photo-album.html" class="btn">View All +</a></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="footer">
    	<script src="js/footer.js"></script>
    </div>
    <!--------------popup-------------->
    <div class="bgPopup"></div>
    <div class="VideoPopup">
    	<div class="closebtn"><a href="javascript:void()"><img src="images/close.png" style="width:30px;"></a></div>
        <h1>Coming Soon</h1>
    </div>
    </div>
</body>
</html>
