<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>।। बाबा रामदास समाधी मंदिर ।। </title>
<!-- Core CSS-->
<link href="css/global.css" rel="stylesheet">
<link href="css/jquery.bxslider.css" rel="stylesheet">
<!--Core Javascript-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.bxslider.js"></script>
<script src="js/main.js"></script>

<script>
$(document).ready(function(){  
  $('.slider').bxSlider();
});
	
</script>
</head>

<body id="insidepage">
	<div class="wrapper">
	<div class="header">
    	<script src="js/header.js"></script>
        <script>$(function(){$('.aPatrika').addClass('active')})</script>
    </div>
    <div class="content">
    	<div class="col1">
        	<div class="program">
            <h1>Program of the year</h1>
            <ul>
            	<li>Ustav</li>                
                <li><a href="pdf/Patrika-Guru-Pornima-2015.pdf" target="_blank">Guru Poornima</a></li>
                <li>Dev Diwali </li>
            </ul>
            <a href="program.html" class="btn">View All +</a>
            
            </div>
            <div class="group">
            	<h1>Hari Hari Group</h1>
                <p>All Devotoes and followers can add
their names here to receive all updates 
on SMS and Email</p>
	<a href="#" class="btn">CLICK TO ADD YOUR DETAILS</a>
            </div>
        </div>
        	<div class="col2">
            <div class="pageHeading"><h1>Patrika</h1></div>
        	 <div class="insidePagecontent">
                    <div class="patrika">
                        <div class="image"><a href="pdf/Patrika-Guru-Pornima-2015.pdf" target="_blank"><img src="images/patrika/patrika.jpg"></a></div>
                        <div class="linkBtn"><a href="pdf/Patrika-Guru-Pornima-2015.pdf" target="_blank"><span class="year">Guru Pornima</span> <img src="images/view-pdf.png"></a></div>
                    </div>
             </div>
             
              
            </div>
            
       
    	<div class="col1">
        	<div class="streaming">
            	<h1><img src="images/icon/live-streaming.png"><span>Live Streaming</span></h1>
            	<div class="video"><a href="javascript:void()"><img src="images/video.jpg"></a></div>
            </div>
            <div class="photogallery">
            <h1>Photo Gallery</h1>
            	<ul class="slider">
                	<li><img src="images/gallery/gallery1.jpg"></li>
                    <li><img src="images/gallery/gallery.jpg"></li>
                    <li><img src="images/gallery/gallery2.jpg"></li>
                    <li><img src="images/gallery/gallery3.jpg"></li>
                    <li><img src="images/gallery/gallery4.jpg"></li>
                    <li><img src="images/gallery/gallery5.jpg"></li>
                </ul>
                <div style="margin-top:-15px"><a href="photo-album.html" class="btn">View All +</a></div>
            </div>
        </div>
    </div>
    
    </div>
    <div class="clear"></div>
    <div class="footer">
    	<script src="js/footer.js"></script>
    </div>
    <!--------------popup-------------->
    <div class="bgPopup"></div>
    <div class="VideoPopup">
    	<div class="closebtn"><a href="javascript:void()"><img src="images/close.png" style="width:30px;"></a></div>
        <h1>Coming Soon</h1>
    </div>
</body>
</html>
